#!/bin/sh
# 
# Set up dotfiles for current user
# 
# - .bashrc
# - .bash_profile
# - .gitconfig
# - .gitignore
# - .vimrc
# - .ssh/config

mv_backup()
{
	mv "$1" "$1-saved-$(date +%Y%m%d-%H%M%S)"
}

# Create .bashrc and .bash_profile
if [ -e `ls ~/.bashrc` ]; then
	mv_backup ~/.bashrc
fi
ln -s ~/shell-config/dotfiles/bash/bashrc ~/.bashrc
echo ". ~/.bashrc" > ~/.bash_profile
echo "~/shell-config/dotfiles/bash/bashrc => ~/.bashrc"

# Create .gitconfig
if [ -e `ls ~/.gitconfig` ]; then
	mv_backup ~/.gitconfig
fi
ln -s ~/shell-config/dotfiles/git/gitconfig ~/.gitconfig
echo "~/shell-config/dotfiles/git/gitconfig => ~/.gitconfig"

# Create .gitignore
if [ -e `ls ~/.gitignore` ]; then
	mv_backup ~/.gitignore
fi
ln -s ~/shell-config/dotfiles/git/gitignore ~/.gitignore
echo "~/shell-config/dotfiles/git/gitignore => ~/.gitignore"

# Create .vimrc
if [ -e `ls ~/.vimrc` ]; then
	mv_backup ~/.vimrc
fi
ln -s ~/shell-config/dotfiles/vim/vimrc ~/.vimrc
echo "~/shell-config/dotfiles/vim/vimrc => ~/.vimrc"

# Create .ssh/config
mkdir -p ~/.ssh
if [ -e `ls ~/.ssh/config` ]; then
	mv_backup ~/.ssh/config
fi
ln -s ~/shell-config/dotfiles/ssh/config ~/.ssh/config
echo "~/shell-config/dotfiles/ssh/config => ~/.ssh/config"

echo
echo 'You may now run ". ~/.bashrc" to load the shell configuration, or "reload" if this is not the first time you run this script.'

# Create ~/.rmate.rc
if [ -e `ls ~/.rmate.rc` ]; then
	mv_backup ~/.rmate.rc
fi
ln -s ~/shell-config/dotfiles/mate/rmate.rc ~/.rmate.rc
echo "~/shell-config/dotfiles/mate/rmate.rc => ~/.rmate.rc"
