#!/bin/sh
# 
# Install vcprompt

mkdir -p ~/src
cd ~/src
hg clone http://vc.gerg.ca/hg/vcprompt/
cd vcprompt
autoconf
./configure
make
ln -s ~/src/vcprompt/vcprompt ~/shell-config/bin
