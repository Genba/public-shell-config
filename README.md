# Shell configuration for UNIX shells

This repository contains configuration files for Bash and common UNIX utilities, like editors (vim) or version control systems (git).

In order to set up your shell with this configuration, clone this repository to your home directory and run the script `~/shell-config/dotfiles/setup.sh`.
